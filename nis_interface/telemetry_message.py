import socket
import socketserver
import threading
import select
import queue
from datetime import datetime

import construct as cs

from .ccsds_time import ccsds_time_to_datetime, datetime_to_ccsds,\
    CCSDS_MICROSECONDS, CCSDS_PICOSECONDS


class TelemetryMessageInterfaceClient:

    def __init__(self, host, port, version, buffer_size=16384):
        self.host = host
        self.port = port
        self.version = version
        self.buffer_size = buffer_size

    def connect(self):
        self.client_thread = threading.Thread(target=self._loop)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))

    def disconnect(self):
        self.socket.close()

    def start(self):
        self.client_thread.kill = False
        self.client_thread.start()

    def stop(self):
        self.client_thread.kill = True
        self.client_thread.join()

    def indication(self, message):
        # to be overwritten by user
        pass

    def _loop(self):
        thread = threading.currentThread()
        buffer = bytearray()

        while not thread.kill:
            try:
                readable, _, _ = select.select([self.socket], [], [], 0)
            except ValueError:
                break

            for sock in readable:
                buffer += sock.recv(self.buffer_size)
                while True:
                    try:
                        message = TelemetryMessage.decode(buffer, self.version)
                    except ValueError:
                        buffer = bytearray()  # clear buffer
                        break
                    buffer = buffer[message.packet_size:]
                    self.indication(message)


class TelemetryMessageInterfaceServer:

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def bind(self):
        self.server = ThreadedTCPServer(
            (self.host, self.port), ThreadedTCPRequestHandler)
        self.server.client_queues = {}
        self.server_thread = threading.Thread(target=self.server.serve_forever)
        self.server.kill = False
        self.server_thread.start()

    def unbind(self):
        self.server.kill = True
        self.server.shutdown()

    def request(self, message):
        for q in self.server.client_queues.values():
            q.put(message)


class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):

    # this method is entered once a client connects
    def handle(self):

        # keep track of connected clients and keep a queue for each
        client_queue = queue.Queue()
        self.server.client_queues[self.client_address] = client_queue

        # run loop to send messages if available in queue
        exit_loop = False
        while True and not self.server.kill and not exit_loop:

            while not client_queue.empty():
                message = client_queue.get()
                try:
                    self.request.send(message.encode())
                except (
                    BrokenPipeError,
                    ConnectionAbortedError,
                    ConnectionResetError
                ):
                    # client disconnected
                    exit_loop = True
        # remove client from dict
        del self.server.client_queues[self.client_address]


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


NisTelemetryMessageV0 = cs.Struct(
    'packet_size' / cs.BytesInteger(4),
    'sc_id' / cs.BytesInteger(2),
    'data_stream_type' / cs.BytesInteger(1),
    'vc_id' / cs.BytesInteger(1),
    'route_id' / cs.BytesInteger(2),
    'ert' / cs.Bytes(8),
    'sequence_flag' / cs.BytesInteger(1),
    'quality_flag' / cs.BytesInteger(1),
    'user_data' / cs.Bytes(cs.this.packet_size - 20)
)


class TelemetryMessage:

    def __init__(
        self,
        version,
        time,
        data,
        packet_size=0,
    ):
        self.version = version
        self.time = time
        self.data = data
        self.packet_size = packet_size

    def encode(self):

        if self.version == 0:
            self.packet_size = 20 + len(self.data)
            ert = datetime_to_ccsds(self.time, format=CCSDS_MICROSECONDS)
            databytes = NisTelemetryMessageV0.build({
                'packet_size': self.packet_size,
                'sc_id': 0,
                'data_stream_type': 0,
                'vc_id': 0,
                'route_id': 0,
                'ert': ert,
                'sequence_flag': 0,
                'quality_flag': 0,
                'user_data': self.data
            })
            return databytes

        elif self.version == 1:
            raise NotImplementedError
        else:
            raise NotImplementedError

    @classmethod
    def decode(cls, pdu, version):
        if len(pdu) == 0:
            raise ValueError("No data to decode")

        if version == 0:
            try:
                container = NisTelemetryMessageV0.parse(pdu)
            except Exception:
                raise ValueError("Parsing error")
            time = ccsds_time_to_datetime(
                container.ert, format=CCSDS_MICROSECONDS)
            data = container.user_data
            packet_size = container.packet_size

        elif version == 1:
            if pdu[0] != 0x81:
                raise ValueError("Version 1 identifier not correct")
            packet_size = int.from_bytes(pdu[1:5], "big")
            ert_format = pdu[13]
            if ert_format == CCSDS_MICROSECONDS:
                ert_length = 8
            elif ert_format == CCSDS_PICOSECONDS:
                ert_length = 10
            else:
                raise NotImplementedError
            ert = pdu[15:15+ert_length]
            time = ccsds_time_to_datetime(ert, ert_format)
            private_annotation_length = pdu[14]
            data = pdu[15+ert_length+private_annotation_length:-1]

        elif version == -1:
            if pdu[0:4] == bytes.fromhex("1acffc1d"):
                data = pdu[4:]  # remove attached sync marker
                time = datetime.utcnow()
                packet_size = len(data)
            else:
                raise ValueError("Attached sync marker not found")

        else:
            raise NotImplementedError

        return cls(
            version,
            time,
            data,
            packet_size
        )
