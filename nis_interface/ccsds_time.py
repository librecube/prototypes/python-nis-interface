from datetime import datetime, timedelta


CCSDS_MICROSECONDS = 41
CCSDS_PICOSECONDS = 42


def ccsds_time_to_datetime(ccsds_time, format):
    EPOCH = datetime(1958, 1, 1)

    if format == CCSDS_MICROSECONDS:
        days_since_epoch = int.from_bytes(ccsds_time[0:2], "big")
        milliseconds = int.from_bytes(ccsds_time[2:6], "big")
        microseconds = int.from_bytes(ccsds_time[6:8], "big")
        seconds = milliseconds / 1e3 + microseconds / 1e6
        return EPOCH + timedelta(days=days_since_epoch, seconds=seconds)

    elif format == CCSDS_PICOSECONDS:
        days_since_epoch = int.from_bytes(ccsds_time[0:2], "big")
        milliseconds = int.from_bytes(ccsds_time[2:6], "big")
        picoseconds = int.from_bytes(ccsds_time[6:10], "big")
        seconds = milliseconds / 1e3 + picoseconds / 1e9
        return EPOCH + timedelta(days=days_since_epoch, seconds=seconds)

    else:
        raise ValueError


def datetime_to_ccsds(dt, format):
    EPOCH = datetime(1958, 1, 1)

    if format == CCSDS_MICROSECONDS:
        deltatime = (dt - EPOCH)
        days_since_epoch = deltatime.days
        total_microseconds = deltatime.seconds * 1e6 + deltatime.microseconds
        milliseconds = int(total_microseconds // 1e3)
        microseconds = int(total_microseconds - milliseconds * 1e3)

        return bytearray([
            (days_since_epoch >> 8) & 0xff,
            days_since_epoch & 0xff,
            (milliseconds >> 24) & 0xff,
            (milliseconds >> 16) & 0xff,
            (milliseconds >> 8) & 0xff,
            milliseconds & 0xff,
            (microseconds >> 8) & 0xff,
            microseconds & 0xff
        ])

    elif format == CCSDS_PICOSECONDS:
        raise NotImplementedError

    else:
        raise ValueError
