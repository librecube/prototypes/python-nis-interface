import socket
import socketserver
import threading
import select
import queue

import construct as cs

from . import logger
from .ccsds_time import ccsds_time_to_datetime, datetime_to_ccsds,\
    CCSDS_MICROSECONDS


class AdminMessageInterfaceClient:

    def __init__(self, host, port, buffer_size=4096):
        self.host = host
        self.port = port
        self.buffer_size = buffer_size

    def connect(self):
        self.client_thread = threading.Thread(target=self._loop)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))

    def disconnect(self):
        self.socket.close()

    def start(self):
        self.client_thread.kill = False
        self.client_thread.start()

    def stop(self):
        self.client_thread.kill = True
        self.client_thread.join()

    def indication(self, message):
        # to be overwritten by user
        pass

    def _loop(self):
        thread = threading.currentThread()

        while not thread.kill:
            try:
                readable, _, _ = select.select([self.socket], [], [], 0)
            except ValueError:
                break

            for sock in readable:
                data = sock.recv(self.buffer_size)
                if len(data) > 0:
                    try:
                        message = AdminMessage.decode(data)
                        self.indication(message)
                    except ValueError as e:
                        logger.warning(e)


class AdminMessageInterfaceServer:

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def bind(self):
        self.server = ThreadedTCPServer(
            (self.host, self.port), ThreadedTCPRequestHandler)
        self.server.client_queues = {}
        self.server_thread = threading.Thread(target=self.server.serve_forever)
        self.server.kill = False
        self.server_thread.start()

    def unbind(self):
        self.server.kill = True
        self.server.shutdown()

    def request(self, message):
        for q in self.server.client_queues.values():
            q.put(message)


class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):

    # this method is entered once a client connects
    def handle(self):

        # keep track of connected clients and keep a queue for each
        client_queue = queue.Queue()
        self.server.client_queues[self.client_address] = client_queue

        # run loop to send messages if available in queue
        exit_loop = False
        while True and not self.server.kill and not exit_loop:

            while not client_queue.empty():
                message = client_queue.get()
                try:
                    self.request.send(message.encode())
                except (
                    BrokenPipeError,
                    ConnectionAbortedError,
                    ConnectionResetError
                ):
                    # client disconnected
                    exit_loop = True
        # remove client from dict
        del self.server.client_queues[self.client_address]


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


NisAdminMessageStruct = cs.Struct(
    'packet_size' / cs.BytesInteger(4),
    'time' / cs.Bytes(8),
    'type' / cs.BytesInteger(2),
    'severity' / cs.BytesInteger(2),
    'event_id' / cs.BytesInteger(4),
    'text' / cs.CString("utf8")
)


class AdminMessage:

    def __init__(
        self,
        time,
        text,
    ):
        self.time = time
        self.text = text

    def encode(self):
        packet_size = 20 + len(self.text) + 1
        time = datetime_to_ccsds(self.time, format=CCSDS_MICROSECONDS)
        databytes = NisAdminMessageStruct.build({
            'packet_size': packet_size,
            'time': time,
            'type': 0,
            'severity': 0,
            'event_id': 0,
            'text': self.text
        })
        return databytes

    @classmethod
    def decode(cls, data):
        if len(data) == 0:
            raise ValueError("No data to decode")

        try:
            container = NisAdminMessageStruct.parse(data)
        except Exception:
            raise ValueError("Parsing error")

        if container.packet_size != len(data):
            logger.debug("packet size [{}] != len(pdu) [{}]".format(
                container.packet_size, len(data)))
            raise ValueError("Packet size not correct")

        time = ccsds_time_to_datetime(
            container.time, format=CCSDS_MICROSECONDS)

        return cls(
            time,
            container.text,
        )
