import time
import logging

from nis_interface import AdminMessageInterfaceClient


logging.basicConfig(level=logging.DEBUG)

host = input("Host [localhost]: ")
host = host if host != "" else "localhost"

port = input("Port [13001]: ")
port = int(port) if port != "" else 13001

client = AdminMessageInterfaceClient(host, port)
client.indication = lambda message: print(message.time, message.text)
client.connect()
client.start()

try:
    while True:
        time.sleep(0.1)
except KeyboardInterrupt:
    pass

client.stop()
client.disconnect()
