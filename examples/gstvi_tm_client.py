import time
import logging

from nis_interface import TelemetryMessageInterfaceClient


logging.basicConfig(level=logging.DEBUG)

host = input("Host [172.16.173.130]: ")
host = host if host != "" else "172.16.173.130"

port = input("Port [13011]: ")
port = int(port) if port != "" else 13011

client = TelemetryMessageInterfaceClient(host, port, version=0)
client.indication = lambda message: print(
    message.time,
    message.data.hex()[:16],
    "...",
    message.data.hex()[-16:],
    )
client.connect()
client.start()

try:
    while True:
        time.sleep(0.1)
except KeyboardInterrupt:
    pass

client.stop()
client.disconnect()
