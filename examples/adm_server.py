import time

from nis_interface import AdminMessageInterfaceServer, AdminMessage


host = input("Host [localhost]: ")
host = host if host != "" else "localhost"

port = input("Port [13001]: ")
port = int(port) if port != "" else 13001

server = AdminMessageInterfaceServer(host, port)
server.bind()

pdu = bytes.fromhex(
    "0000004d5ab1029c8c700000000100000000000145737461626c6973686564205443206c6"
    "96e6b20746f20534c455f50524f563a207365712e20636f756e74203d2020202020202020"
    "20203100"
)
message = AdminMessage.decode(pdu)

try:
    while True:
        time.sleep(1)
        server.request(message)
except KeyboardInterrupt:
    pass

server.unbind()
